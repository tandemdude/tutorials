# Embeds

Now we're gonna learn all about embeds!

Here's how an embed looks like:

![Embed Breakdown](./imgs/embed_breakdown.png)

Now, you see, embeds are very straight forward. To make them, we need to use the class `EmbedBuilder` like this:

```java
EmbedBuilder eb = new EmbedBuilder();
```

Here, we made a new `EmbedBuilder` to build an embed. Now let's see how to change the color, add fields, add a thumbnail, add a picture, and add an author, etc.

```java
eb.setColor(0xFF00FF);
// This will change the color of the embed.
```

This is used to change the color of the embed. You need to enter a hex, and switch the `#` with `0x` so if you need to make it white, instead of `#FFFFFF` you'd type `0xFFFFFF`. That syntax is used in many languages to signify hex numbers.

```java
eb.setTitle("this is a title!");
// This will set the title of the embed to `this is a title!`.
```

This will set the title of the embed to the string inside the parentheses. If I wanted the title to say "Woo!" then I'd set the title of the embed to `eb.setTitle("Woo!")`. It's that easy!

```java
eb.setDescription("This is a description!");
// This will change the description of the embed to This is a description!
```

This will set the description to "This is a description!". The description is under the title of the embed.

```java
eb.setAuthor("M.A", "iconurl");
// This sets the name of the author to M.A, and sets the icon of the author to a URL!
```

This sets the `author` attribute name to "M.A", and the icon to a URL. Be sure to a valid URL for Discord or it won't work.

```java
eb.setThumbnail("url");
// sets the thumbnail to a picture from a URL
```

This sets the thumbnail to a picture from the URL. Be sure to use a valid URL for Discord or it won't work.

```java
eb.addField("name of the field", "value of the field", False); // or you can put True instead
// you can choose True or False in the inline field (the 3rd argument, could use True or False, I did False)
```

This is an attribute that adds a field to your embed, it has 2 strings, and an inline boolean.

Here's an example on inline and non-inline fields:

![Embed Inline Fields](./imgs/embed_inline_fields.png)

As you can see, the inline fields are all in one line,

![Embed Non-Inline Field](./imgs/embed_non-inline_field.png)

But the non-inline field takes another line.

The 2 strings `name` and `value` both show up in the example shown, the name being the bold and upper one, the value being the little transparent one.

![Embed Inline Empty Field](./imgs/embed_inline_empty_field.png)

Also, in JDA, you can add empty fields!
To add empty fields, you should do:

```java
eb.addBlankField(True);
```

That'll make the field inline so its in the same line,
> NOTICE: You can only put 3 inline fields at one line.

![Embed Non-Inline Empty Field](./imgs/embed_non-inline_empty_field.png)

This shows a blank non-inline field!
To do that, the code would be something like this

```java
eb.addBlankField(False);
```

Thats all about blank fields! Now, moving on, to **Embed Images!**

This is pretty easy, you just need to do:

```java
eb.setImage("url");
```

The URL must be valid for Discord to view.

Now, onto footer!

![Embed Footer](./imgs/embed_footer.png)

This is a footer, its made using this:

```java
eb.setFooter("this is a footer", "iconurl");
```

This is a how you set the Footer, again, with the `iconURL` starting with `https`/`http` or with `attachment://`

![Embed Timestamp](./imgs/embed_timestamp.png)

Now, into timestamps!

Timestamps are made in JDA with:

```java
eb.setTimestamp(Instant.now());
```

This will make the timestamp take the exact time of when the embed was made and sets as the timestamp!

That will set the attributes of the embed, but not put it to a message and post the message! To do that, we will code the following:

```java
MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());
messageChannel.sendMessage(message.build().queue();
```

> NOTICE: you can also chain these calls! For example:

```java
EmbedBuilder eb = new EmbedBuilder()
     .setTitle("This is a title :blush:")
     .setDescription("I'm kewl")
     .setColor(0xC0FFEE)

// then later on we can do this

MessageBuilder message = new MessageBuilder();

message.setEmbed(eb.build());

MessageChannel.sendMessage(message.build().queue();
```

Thats all about embeds!

---

Finally, to [**ReadyEvent and Logging**](./readyevent-and-logging.html)!
