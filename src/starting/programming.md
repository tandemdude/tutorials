# Programming tutorial
***by `M.A#4999`***

Hello there! This is a tutorial about programming for beginners that links to sources and ways to learn how to deal with programming languages, APIs, and how to work with them.

Naturally, when you want to make something that includes programming, e.g. a game, or a Discord bot (what mainly this tutorial would be focusing on), You'll ask:

> Help! I want to make a (Discord bot/game).

And, well, the first question you're going to get asked is, "What language are you planning to write it in?", And surprisingly, the answer isn't "English" :)

See, what people mostly mean by language, is a [Programming Language](https://google.com/search?q=Programming+Language+Definition)!

Currently, there is a great number of programming languages, which is why you need to choose one.

See, they were called languages because you're communicating with the computer with it; there are multiple languages that you will be able to use.

The three most used and common, are [Python](https://google.com/search?q=Python+Programming+Language), [Java](https://google.com/search?q=Java+Programming+Language), and [JavaScript](https://google.com/search?q=JavaScript+Programming+Language) (mainly [NodeJS](https://google.com/search?q=NodeJS)).

So, what are the ways you can learn the languages? Well, there is an extremely HUGE amount of tutorials for some languages, and one way to find them is [Google](https://google.com/)! See, when becoming a programmer, Google becomes something crucial within your work, to know why some errors are happening and how to stop them.

A great way to learn a programming language may be just googling its name, and write tutorials after, e.g. [Python Tutorial](https://google.com/search?q=Python+Tutorial), [Java Tutorial](https://google.com/search?q=Java+Tutorial) and [JavaScript Tutorial](https://google.com/search?q=JavaScript+Tutorial) for Python. But! The ones we recommend are [Learn Python in Y minutes](https://learnxinyminutes.com/docs/python3/), [Codeacademy's course for Python](https://www.codecademy.com/learn/learn-python), and [Reading the docs](https://docs.python.org/3)!

> Note #1: docs is a shortening of "documentation," which is a word that describes pages of text written to provide information or evidence. You can use "manual" instead!

> Note #2: Versions are a very important thing in programming, you might ALMOST ALWAYS want to update to the latest version of something, unless it has errors that make it unusable/worse than the older version, or if it has bugs that may worsen the language. If dealing with famous, or software that has extremely good owners, the bugs and errors will be fixed in another version. This is why we recommend you use Python 3.6 and higher :)

Ones we recommend for Java, [Codeacademy's Java Course](https://www.codecademy.com/learn/learn-java), and [CodingBat's Java Course](https://codingbat.com/), Which may be a good source to learn some Java programming with. (CodingBat also has Python, but it is an old version, please read the second note above to know what's wrong with it, it uses Python 2.7 which is an old version that doesn't have many software and API versions that support it, that's why we recommend you to update your version if it isn't the latest already).

Ones we recommend for JavaScript are [W3Schools' JavaScript Course](https://www.w3schools.com/js/), and [The Modern JavaScript Tutorial](https://javascript.info).

And if you just want generic problems to solve there's always [AdventOfCode](https://adventofcode.com), which comes packed with a lot of interesting problems that scale up the difficulty the more you progress.
If you want to go on a more mathematical way, you can always check [ProjectEuler](https://projecteuler.net/).

Now that you have your sources to learn programming, you need ways to test code that you may want to run and make incredible projects! For that, we present IDEs and text editors, both being an important part of a programmer's computer.

One of the text editors I will recommend is [Sublime Text 3](https://www.sublimetext.com/3), There are other text editors, such as [Atom](https://atom.io/), and [Brackets](http://brackets.io/) (Even though it was built for web developers, it will be helpful too).

> Note #3: On September 1, 2021, Adobe will end support for Brackets. This means Brackets won't receive any updates after that so it might not be the best choice.

Now: IDEs, an IDE stands for [Integrated Development Environment](https://google.com/search?q=IDE), and what IDE you should use is dependent on what programming language you'll use, and there are IDEs that you can use for any programming language, such as [Visual Studio Code](https://code.visualstudio.com/).

One of the best IDEs for Python would be [PyCharm](https://www.jetbrains.com/pycharm/).

One of the best IDEs for Java would be [IntelliJ IDEA](https://www.jetbrains.com/idea/).

Now for JavaScript, there aren't really many free IDEs, all of the above IDEs mentioned do support JavaScript, [Visual Studio Code](https://code.visualstudio.com/) being frequently used by JavaScript developers, another IDE for JavaScript that has a 30-day free trial, then you'll have to pay for, is [WebStorm](https://www.jetbrains.com/webstorm/).

Please note that you can use IntelliJ IDEA and PyCharm interchangeably, too!

Now, after you've learned for at least some months with testing, making projects, googling, and learning, You might be able to make a Discord bot! But now... How?

See, now, you'll need to use an API, which stands for an [Application Programming Interface](https://en.wikipedia.org/wiki/API).

APIs are what make a program, you use them to make almost anything in programming, even the main language is an API.

And, to make a Discord bot, you'll need to interact with Discord's API using the programming language!

You know, that's really gonna end up to be messy and confusing for starters to make by yourself, and that's why people already made some good APIs that will help!

Mainly, for Python, [discord.py by Danny](https://github.com/Rapptz/discord.py/blob/master/README.rst) is used! Though there are other libraries, like [Hikari by Nekokatt](https://github.com/nekokatt/hikari/blob/master/README.md) for example. It is a new library that was made to be safer and more reliable than discord.py, with sane standards. It also has a command handler written for it: [Lightbulb by Thomm.o](https://gitlab.com/tandemdude/lightbulb/-/blob/development/README.md) which is similar to discord.py's.

Mainly, for Java, there are 3 APIs made, [JDA made by DV8FromTheWorld](https://github.com/DV8FromTheWorld/JDA/blob/master/README.md), [JavaCord made by Bastian](https://github.com/Javacord/Javacord/blob/master/README.md) and [Discord4J made by Austin](https://github.com/Discord4J/Discord4J/blob/master/README.md).

JavaScript has mainly 2 popular long-standing APIs, [Discord.js by Hydrabolt](https://github.com/discordjs/discord.js/blob/master/README.md) and [Eris by abalabahaha](https://github.com/abalabahaha/eris/blob/master/README.md).

And you may find installation ways in the README.md :)

> Note #3: There are more programming languages and more Discord APIs. Go, Rust, Julia, Lua, Scala each have an API or more :)

You can google anything you need or something you don't know, and you can even join our [Discord server](https://discord.gg/RDKfMcC) and ask us stuff! See you later! :D
