# Starting with aioredis

So, you got your pretty Redis server up and properly configured and now you need to use it! In this section I'll go over how to setup a Redis connection using the [`aioredis`](https://pypi.org/project/aioredis) Python module.

So first, we need the Redis server address and any other option you might have put in the config file. I'll make a connection using the config file I showed as an example in [**Setting up the server**](./setting-up-the-server.html).

The address is `redis://localhost` because we are running the server on the same machine as our app. Then I set a password as `asdfmoviesarecool` and finally I want to connect to the 3rd db index and use `utf-8` as the enconding. This will result in the following arguments for the `aioredis.create_redis_pool()` function.
```py
redis_conn = await aioredis.create_redis_pool(
    "redis://localhost",  # this is the address
    password = "asdfmoviesarecool",  # password
    db = 2,  # third db index (it starts on 0)
    encoding = "utf-8")  # encoding
```
*__Note__: Please refer to [Final Notes](./final-notes.html) where I cover how to hide your arguments so that you don't accidentally leak your address or password.*

We are now ready to do operations with the Redis instance. Let me show you how to incorporate it into a discord.py bot.
```py
@bot.listen() 
async def on_start():
    """Fire up the Redis connection pool"""
    try:
        # Use the high-level interface.
        db_pool = await aioredis.create_redis_pool(
            # pass your arguments as I've shown in the previous code block 
        )
        setattr(bot, "db_pool", db_pool) # setting a pool attribute for the bot variable so you can use it anywhere like bot.db_pool...
        logger.info(f'Created pool to {db_args["address"]}') # this can be replaced by a print() 
    except:
        logger.error(f'Could not start database pool with error:\n{traceback.print_exc()}\n. Continuing without it')
```

Now let's setup the closing even. This is important to prevent leaving pending connections. 
```py
@bot.listen()
async def on_logout():
    """Close the connection pool when the bot exits."""
    try:
        bot.db_pool.close()
        logger.warning('Closed the database connection pool')
    except:
        logger.error(f'Could not close database pool with error:\n{traceback.print_exc()}\n. Continuing without it') 
```

Here I'm using a modified client from [`libneko`](https://gitlab.com/Tmpod/libneko/) which adds some neat stuff, including two new events: `on_start` and `on_logout`.
